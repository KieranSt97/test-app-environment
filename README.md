# test-app-environment

Environment definition for the python test-app

I have chosen option one for this DevOps tech test which was;

1. Deploy this code somewhere, then write a brief description of what you've done, and what you'd do given more time and resources. We don't care where or how you deploy it, we're interested in you demonstrating your fundamental skills.

I decided to deploy this python application into AWS Fargate, the serverless version of the Elastic Container Service using Terraform and Gitlab CI. I spent a little longer than an hour on this as there was a tiny bit of refactoring to do with the python code but I feel like what has been done adds a lot of value.

The first step was to manually containerize the application and push it to AWS ECR (Elastic Container Registry). The Dockerfile used to build the container is in this repository and I used the below commands to build and push it to ECR.

- `docker build -t pythonapp:latest .`
- `eval $(aws ecr get-login --no-include-email --region eu-west-2 | sed 's|https://||')`
- `docker tag pythonapp:latest 0000000000.dkr.ecr.eu-west-2.amazonaws.com/pythonapp:latest`
- `docker push 0000000000.dkr.ecr.eu-west-2.amazonaws.com/pythonapp:latest`

I then set up a very simple three stage Gitlab CI pipeline containing the following stages. 

- `terraform validate`
- `terraform plan`
- `terraform apply (manual stage)`

These stages all perform a `terraform init` using a remote S3 backend with DynamoDB for locking and also a `terraform workspace select $CI_COMMIT_REF_NAME` using the branch name and if the workspace doesn't already exist it gets created. 

I then wrote some very quick and simple Terraform code to stand up the following resources. Please note that none of the Terraform parameters are externalized and no reusable modules were written/used in order for me to speed up the process. 

- `ECS Cluster`
- `ECS Service`
- `Application Loadbalancer`
- `IAM Role` 
- `Security groups`
- `Task definition`

Upon a successful pipeline run and environment build I am able to access the application using the loadbalancer DNS, for example..  
http://testapp-loadbalancer-0000000000.eu-west-2.elb.amazonaws.com/instructions

If I had more time on this I would have;

- Created a different repository and added CI to build and push the docker image to ECR. 
- Added certificates to the loadbalancer and enforced TLS throughout the stack. 
- Tightened up the security group rules and IAM roles.
- Written and or used Terraform modules to add reusability and consistency. 
- Add more stages to the CD pipeline for example. terraform linting, terraform format and a smoke test once the application was deployed.
- Register a domain name on Route53 and create a record set pointing this domain to the DNS of the loadbalancer.
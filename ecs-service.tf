resource "aws_ecs_service" "ecs_service" {
  name             = "testapp-ecs-service"
  cluster          = aws_ecs_cluster.ecs_cluster.id
  task_definition  = aws_ecs_task_definition.ecs_task_definition.arn
  desired_count    = 1
  launch_type      = "FARGATE"

  load_balancer {
    target_group_arn = aws_lb_target_group.lb_target_group.arn
    container_name   = "python-testapp"
    container_port   = 5000
  }

  network_configuration {
    subnets         = ["subnet-09449573","subnet-a5dc3be9","subnet-2580144c"]
    security_groups = [aws_security_group.security_group_ecs.id]
    assign_public_ip = true
  }

}

resource "aws_ecs_task_definition" "ecs_task_definition" {
  family                   = "python-testapp"
  container_definitions    = data.template_file.task_definition.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 512
  memory                   = 1024
  execution_role_arn       = aws_iam_role.iam_role_ecs.arn
  task_role_arn            = aws_iam_role.iam_role_ecs.arn
}

resource "null_resource" "lb_exists" {
  triggers = {
    alb_name = aws_lb.lb.name
  }
}

resource "aws_lb_target_group" "lb_target_group" {
  depends_on  = [null_resource.lb_exists] #Required to ensure ALB exists before provisioning target group
  name        = "testapp-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = "vpc-419a1d29"
  target_type = "ip"

  health_check {
    enabled             = true
    interval            = 30
    path                = "/"
    port                = 5000
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
    matcher             = 200
  }
}

resource "aws_lb_listener_rule" "lb_listener_rule" {
  listener_arn = aws_lb_listener.lb_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }

}

resource "aws_security_group" "security_group_ecs" {
  name        = "testapp-ecs-security-group"
  vpc_id      = "vpc-419a1d29"
}

resource "aws_security_group_rule" "security_group_rule_ecs_in" {
  type                     = "ingress"
  from_port                = 5000
  to_port                  = 5000
  protocol                 = "tcp"
  security_group_id        = aws_security_group.security_group_ecs.id
  source_security_group_id = aws_security_group.security_group_alb.id
  description              = "Inbound from ALB to ECS service"
}

resource "aws_security_group_rule" "security_group_rule_ecs_in_ecr_endpoint" {
  type                     = "ingress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.security_group_ecs.id
  source_security_group_id = "sg-5648183d"
  description              = "Inbound from ECR Endpoint to pull images"
}

resource "aws_security_group_rule" "security_group_rule_ecs_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.security_group_ecs.id
  cidr_blocks       = ["0.0.0.0/0"]
}

data "template_file" "task_definition" {
  template = file("templates/service.json")
  vars = {
    branch         = terraform.workspace
    cloudwatch_log_group = aws_cloudwatch_log_group.cloudwatch_log_group.name
  }
}

resource "aws_cloudwatch_log_group" "cloudwatch_log_group" {
  name = "testapp-log-group"
}
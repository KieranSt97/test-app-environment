resource "aws_iam_role" "iam_role_ecs" {
  name               = "testapp-ecs-iam-role"
  assume_role_policy = data.aws_iam_policy_document.iam_policy_document_ecsassumerole.json
}

data "aws_iam_policy_document" "iam_policy_document_ecsassumerole" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com", "ecs.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "iam_role_policy_attachment_ecstaskexec" {
  role       = aws_iam_role.iam_role_ecs.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
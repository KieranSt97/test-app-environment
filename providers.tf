provider "aws" {
  version = "~> 2.65.0"
  region  = "eu-west-2"
}
terraform {
  backend "s3" {
    key     = "env.tfstate"
    region  = "eu-west-2"
    encrypt = true
  }

  required_version = "= 0.13.5"
}
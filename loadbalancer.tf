resource "aws_lb" "lb" {
  name               = "testapp-loadbalancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.security_group_alb.id]
  subnets            = ["subnet-09449573","subnet-a5dc3be9","subnet-2580144c"]

  enable_deletion_protection = false
}

resource "aws_lb_listener" "lb_listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content"
      status_code  = "200"
    }
  }
}

resource "aws_security_group" "security_group_alb" {
  name        = "alb-security-group"
  vpc_id      = "vpc-419a1d29"
}

resource "aws_security_group_rule" "security_group_rule_alb_in" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group_alb.id
}

resource "aws_security_group_rule" "security_group_rule_alb_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.security_group_alb.id
  cidr_blocks       = ["0.0.0.0/0"]
}